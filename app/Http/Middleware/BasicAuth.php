<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuth
{

    public function handle($request, Closure $next)
    {

        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $suppliedCredentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $notAuthenticated = (
            !$suppliedCredentials ||
            $_SERVER['PHP_AUTH_USER'] != env('AUTH_USERNAME') ||
            $_SERVER['PHP_AUTH_PW']   != env('AUTH_PASSWORD')
        );
        if ($notAuthenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }

        return $next($request);
    }
}
