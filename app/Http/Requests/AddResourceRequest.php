<?php


namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class AddResourceRequest extends FormRequest
{


    public function rules()
    {

        return [
            'url' => 'required|min:3',
        ];


    }
}