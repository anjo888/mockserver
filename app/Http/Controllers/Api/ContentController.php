<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Services\ContentServiceInterface;
use Illuminate\Http\Request;

class ContentController
{
    public $service;

    public function __construct(ContentServiceInterface $service)
    {
        $this->service = $service;

    }

    /**
     * @param Request $request
     *
     * @return mixed|string
     */
    public function getContent(Request $request)
    {
        $query = $request->input('q');

        return $this->service->getContent($query) ?? view('nocontent');
    }
    /**
     * @OA\Info(
     *     version="1.0",
     *     title="Meta"
     * )
     */
    /**
     * @OA\Get(
     *   path="/api/meta/{url}",
     *   summary="Get metadata",
     *   @OA\Parameter(
     *     in="path",
     *     name="url",
     *     required=true,
     *     @OA\Schema(
     *     type="integer",
     *     ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\MediaType(
     *     mediaType= "aplication/json",
     *     @OA\Schema(
     *     type = "object",
     *     @OA\Property(property="status", type="string", description="OK"),
     *     @OA\Property(property="message", type="string", description="Metadata has been added"),
     *     @OA\Property(property="data", type="array",
     *     @OA\Items(type="object",
     *     @OA\Property(property="url", type="string"),
     *     @OA\Property(property="content_id", type="string"),
     *     @OA\Property(property="content_length", type="integer"),
     *     @OA\Property(property="content_type", type="string"),
     *     @OA\Property(property="http_status", type="integer"),
     * ),
     *     description="Message"),
     *     )
     * )
     * ),
     *   @OA\Response(
     *     response=400,
     *     description="Validation url error",
     * )
     *     )
     */
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function getMeta(Request $request)
    {
        $meta = $request->input('meta');
        $result = $this->service->getMeta($meta);
        if ($result !== null) {
            return response()->json(['status' => 'ok', 'message' => 'Metadata has been added', 'data' => $result]);
        }

        return response()->json(['status' => 'error', 'message' => 'No metadata', 'data' => ['url' => $meta]]);
    }

}