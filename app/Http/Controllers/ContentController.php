<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddResourceRequest;
use App\Interfaces\Services\ContentServiceInterface;


class ContentController extends Controller
{
    public $service;

    /**
     * ContentController constructor.
     *
     * @param ContentServiceInterface $service
     */
    public function __construct(ContentServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param AddResourceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function addResource(AddResourceRequest $request)
    {
        $url = $request->input('url');

        $result = $this->service->saveContent($url);

        if ($result !== null) {

            return response()->json(['status' => 'ok', 'message' => 'Url has been added', 'data' => $result]);
        }

        return response()->json(['status' => 'error', 'message' => 'Error', 'data' => ['url' => $url]]);
    }




}
