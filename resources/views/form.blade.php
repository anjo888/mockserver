@extends('app')
@section('title')
    <title>Добавление контента</title>
@stop
@section('form')
    <h2>Добавление контента</h2>

    {!! Form::open(['route'=> 'saveContent' ]) !!}
    <div class="form-group">
        {!! Form::text('url', null,['class'=>'form-control', 'id'=>'url']) !!}
    </div>
    {!! Form::submit('Добавить',['class'=>'btn btn-primary' ]) !!}
    {!! Form::close() !!}


    @if($errors->any())
        <ul class=" alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@stop
