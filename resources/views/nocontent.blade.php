@extends('app')
@section('title')
    <title>Просмотр контента</title>
@stop

@section('form')
    <h2>Просмотр контента</h2>
    <form class="form-inline" action="{{route('getContent')}}" method="get" enctype="multipart/form-data">
        <div class="form-group">
            <input class="form-control" type="url" name="q" placeholder="Url" required>
            <button type="submit" class="btn btn-primary">Просмотр</button>
        </div>
    </form>
@stop

